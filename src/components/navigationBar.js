import React from 'react';
import NavigationButton from './navigationButton';

const sites = [{
        id: 1,
        name: 'Wolfram Neugebauer'
      }, {
        id: 2,
        name: 'Remise Hamburg'
      }, {
        id: 3,
        name: '4U-TEAM'
      }, {
        id: 4,
        name: 'SABINE SCHWER'
      }, {
        id: 5,
        name: 'DIANA OTTO'
      }];


class NavigationBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 1
    }
  }

  handleChange(value) {
    this.setState({value});
  }

    render() {
      return(
        <div>
        {sites.map((site, i) => {
          return (
          <NavigationButton key={site.id}
                            index={i}
                            id={site.id}
                            site={site.name}
                            handleChange={this.handleChange.bind(this, site.id)}
                            primary={this.state.value === site.id ? true : false}
          />
          );
        })}
        </div>
      );
    }
}

export default NavigationBar;
