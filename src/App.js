import React, { Component } from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import NavigationBar from './components/navigationBar';
import MainContainer from './containers/mainContainer';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();


class App extends Component {

  render() {
    return (
      <MuiThemeProvider>
      <div className="App">
      <AppBar
      title="HOF 21 Administration"
      showMenuIconButton={false}>
      <div style={{color:'white', fontSize: '13pt', marginTop:'20px'}}><a style={{textDecoration: 'none', color: 'white'}} href='#'>Logout</a></div>
      </AppBar>
      <NavigationBar />
      <MainContainer />
      </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
