import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';


export default class NavigationButton extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleMouseEnter = (event) => {
    event.preventDefault();
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleMouseLeave = (event) => {
    this.setState({
      open: false,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    return (
          <RaisedButton
          onMouseEnter={this.props.primary ? this.handleMouseEnter : null}
          onClick={this.props.handleChange}
          label={this.props.site}
          primary={this.props.primary}>
          <Popover
            open={this.state.open}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            onRequestClose={this.handleRequestClose}
            animation={PopoverAnimationVertical}>
            <Menu onMouseLeave={this.handleRequestClose}>
              <MenuItem onClick={this.handleRequestClose} primaryText="INTERIEUR" />
              <MenuItem onClick={this.handleRequestClose} primaryText="DESIGN" />
              <MenuItem onClick={this.handleRequestClose} primaryText="INFORMATION" />
              <MenuItem onClick={this.handleRequestClose} primaryText="KONTAKT" />
            </Menu>
          </Popover>
          </RaisedButton>
    );
  }
}
