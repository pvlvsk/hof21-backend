import React from 'react';
import {Card, CardActions, CardHeader, CardTitle} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FontIcon from 'material-ui/FontIcon';

const style = {
  height: 250,
  width: 180,
  margin: 20,
  padding: 0,
  textAlign: 'center',
  display: 'inline-block',
  cursor: 'pointer'
};

const MOCK_URL = 'http://stage.wolframneugebauer.de/admin/upload/files/standard/';

const posts = [{
        id: 1,
        url: 'Komode_ArneFodder.jpg'
      }, {
        id: 2,
        url: 'LampeSchottlander.jpg'
      }, {
        id: 3,
        url: 'Weißenhofstuhl_2.jpg'
      }, {
        id: 4,
        url: 'Sessel_ArneJacobsen_01-web.jpg'
      }, {
        id: 5,
        url: 'Sessel_PoulKjaerholm_2.jpg'
      },
      {
        id: 6,
        url: 'Linoliumtisch.jpg'
      }, {
        id: 7,
        url: 'Lederstuhl_2.jpg'
      },
      {
        id: 8,
        url: 'TeakHolz_Sessel.jpg'
      }, {
        id: 9,
        url: 'Schaukelstuhl_1.jpg'
      },
      {
        id: 10,
        url: 'Ledersessel_PoulKjaerholm_1.jpg'
      }, {
        id: 11,
        url: 'Ledersessel_PoulKjaerholm_2.jpg'
      },
      {
        id: 12,
        url: 'LampenSchottlander.jpg'
      }, {
        id: 13,
        url: 'Keramikbueste_1.jpg'
      },
      {
        id: 14,
        url: 'Afrikamaske_30cm.jpg'
      }];

class CardExampleWithAvatar extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      edit_post: 0,
      snack_open: false
    }
  }

  showDialog(id) {
    this.setState({ edit_post: id });
    this.setState({ open: true });
    this.setState({ snack_open: false });
  }

  hideDialog = () => {
    this.setState({ open: false });
    this.showSnackbar();
  }

  showSnackbar = () => {
    this.setState({ snack_open: true });
  }

  render() {
    const actions = [
      <FlatButton
        label="Bild löschen"
        primary={true}
        onTouchTap={this.hideDialog}
        style={{float: 'left', color:'red'}}
      />,
      <FlatButton
        label="Speichern"
        primary={true}
        onTouchTap={this.hideDialog}
      />
];
    return(
      <Card style={{boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 0px 0px rgba(0,0,0,0.24)'}}>
        <CardHeader style={{textAlign: 'left', paddingBottom: '40px'}}
          title="WOLFRAM NEUGEBAUER"
          subtitle="http://www.wolframneugebauer.de"
        >
        </CardHeader>
        <CardTitle style={{marginTop: '-90px'}} title="INTERIEUR" subtitle="Letzte Änderung: 11.09.2016 8:45">
          <a href="#"><FontIcon className="material-icons" style={{fontSize:'40pt', marginRight: '200px', marginTop: '-62px', color: '#cccccc'}}>arrow_back</FontIcon></a>
          <a href="#"><FontIcon className="material-icons" style={{fontSize:'40pt', marginLeft: '200px', marginTop: '-62px', color: '#cccccc'}}>arrow_forward</FontIcon></a>
        </CardTitle>

        <CardActions style={{float: 'right', marginTop: '-75px'}}>

        <FloatingActionButton className="save" style={{marginRight: '20px'}}>
        <FontIcon className="material-icons" style={{fontSize:'40pt', marginLeft: '200px', marginTop: '-62px', color: '#cccccc'}}>done</FontIcon>
      </FloatingActionButton>
        <FloatingActionButton style={{marginRight: '20px'}}>
        <ContentAdd />
      </FloatingActionButton>
        </CardActions>
        <div>
        {posts.map((post, i) => {
          return (
          <Paper
          style={style}
          zDepth={1}
          onClick={this.showDialog.bind(this, i)}
          key={post.id}
          index={i}
          id={post.id}>
          <img alt="none" style={{height: 250, width: 180, objectFit: 'cover'}} src={MOCK_URL+posts[i].url} />
          </Paper>
          );
        })}
           <Dialog
             title={posts[this.state.edit_post].url}
             actions={actions}
             modal={false}
             open={this.state.open}
             onRequestClose={this.handleClose}
             contentStyle={{width: '25vw', height: '100vh', textAlign: 'center'}}
           >
             <img alt="none" style={{width: '25vw', marginLeft: '-24px'}} src={MOCK_URL+posts[this.state.edit_post].url}/><br/><br/>
             <TextField
             hintText="Breuer Liege // Eisengarn // € 1.712"
             defaultValue="Breuer Liege // Eisengarn // € 1.712"
             multiLine={true}
             rows={1}
             rowsMax={4}
             />
           </Dialog>
           <Snackbar
           style={{color: '#ccc'}}
          open={this.state.snack_open}
          message="Änderungen gespeichert!"
          autoHideDuration={2000}
          onRequestClose={this.handleRequestClose}
        />
         </div>

      </Card>
    );
  }

}

export default CardExampleWithAvatar;
